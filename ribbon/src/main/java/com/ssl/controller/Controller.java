package com.ssl.controller;

import com.ssl.service.RibbonService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ribbon")
public class Controller {

    private RibbonService ribbonService;

    public Controller(RibbonService ribbonService) {
        this.ribbonService = ribbonService;
    }

    @GetMapping("/hello")
    public String test(){
        return ribbonService.port();
    }
}
