package com.ssl.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RibbonService {
    private RestTemplate restTemplate;

    public RibbonService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String port() {
        return restTemplate.getForObject("http://producer/demo/provider", String.class);
    }
}
