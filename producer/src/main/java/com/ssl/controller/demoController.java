package com.ssl.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class demoController {
    @Value("${server.port}")
    private String port;

    @GetMapping("/provider")
    public String helloProvider() {
        return "I am is service ,hello world " + port;
    }
}
