package com.ssl.rest;


import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "producer")
public interface HelloRemote {

    //这个接口要和远程调用的接口一只，参数，接口名，返回类型
    @RequestMapping(value = "/demo/provider")
    String helloProvider();

}
