package com.ssl.controller;

import com.ssl.rest.HelloRemote;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/demo")
public class demoController {

    private HelloRemote helloRemote;

    private RestTemplate restTemplate;

    @Value("${hello.url}")
    private String helloUrl;

    public demoController(HelloRemote helloRemote, RestTemplate restTemplate) {
        this.helloRemote = helloRemote;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/hello")
    public String hello() {
        return helloRemote.helloProvider();
    }

    //RestTemplate方式调用
    @RequestMapping(value = "/hello2")
    public String hello2() {
        return restTemplate.getForEntity(helloUrl, String.class).getBody();
    }
}
